import serial, time, logging


class RS485(object):
    def __init__(self, cfg):
        self.log = logging.getLogger('rs485')
        port = cfg['port']
        baudrate = cfg['baudrate']
        timeout = 0.05
        write_timeout = 0.3
        if 'timeout' in cfg:
            timeout = cfg['timeout']
        if 'write_timeout' in cfg:
            write_timeout = cfg['write_timeout']
        self.serial = serial.Serial(port=port, baudrate=baudrate, timeout=timeout, writeTimeout=write_timeout)
        self.log.info('{} is opened. baudrate={}, timeout={}, writeTimeout={}'.format(self.serial.port,
                                                                                      self.serial.baudrate,
                                                                                      self.serial.timeout,
                                                                                      self.serial.writeTimeout))

    def close(self):
        self.serial.close()

    def read_until(self, char=None):
        def reader():
            while True:
                # t1 = time.time()
                tmp = self.serial.read(1)
                # t2 = time.time()
                if not tmp or (char and char == tmp): break
                # print('----------- {0:0.6f}'.format(t2 - t1))
                yield tmp

        return b''.join(reader())

    def read_waiting(self, char=None):
        def reader():
            while self.serial.inWaiting():
                tmp = self.serial.read(self.serial.inWaiting() or 1)
                if not tmp or (char and char == tmp): break
                yield tmp

        return b''.join(reader())

    def query(self, qry, timeout=0):
        try:
            bask = bytearray(qry + '\r', encoding='utf-8')
            self.serial.write(bask)
            self.serial.flush()
            if timeout:
                time.sleep(timeout)
            bstr = self.read_until(b'\r')
            if len(bstr) != 0:
                return bstr.decode(encoding='utf-8')
        except:
            pass
        self.log.warning('query error')
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
        return None

# cat /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
# 16
# echo 1 > /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
# cat /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
# 1
# reading kernel sources (4.11) can set the same thing from the shell (setserial /dev/ttyUSB0 low_latency), which uses the TIOCSSERIAL ioctl.
