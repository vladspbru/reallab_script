@echo off

set PATH=C:\Python\Python35-32\Scripts\;C:\Python\Python35-32\;


if "%1"=="wenv" (
	echo "creating WindowsENV"

	virtualenv wenv
	set PATH=.\wenv\scripts;
	pip install --upgrade pip setuptools
	pip install -r requirements.txt
) 




set APP=adam_pump
if "%1"=="" (
	echo "creating %APP%"

	set PATH=.\wenv\scripts;
	pyinstaller --console --onefile --icon "favicon.ico"  --name=%APP% adam.py 
	rd /s /q build
	rd /s /q __pycache__
	erase *.spec
)





