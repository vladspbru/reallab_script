#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# __author__ = 'vladislav'


import rs485
import os
import sys
import time
from datetime import datetime
import yaml
import json
import logging
# import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish


class Statistic(object):
    def __init__(self):
        self.clean()

    def clean(self):
        self.time = 0
        self.tick = 0
        self.errors = 0
        self.dt = [0, 0, 0]
        self.bad_qry = 0

    def __repr__(self):
        return json.dumps(self.__dict__)

    def pass_to_mqtt(self, mqtt_cfg):
        if mqtt_cfg:
            try:
                topic = "{}{}".format(mqtt_cfg["topic"], "$sys")
                msgs = []
                msgs.append(dict(topic=topic, payload=self.__repr__()))
                publish.multiple(msgs
                                 , hostname=mqtt_cfg["host"]
                                 , port=mqtt_cfg["port"]
                                 , client_id=mqtt_cfg["client_id"]
                                 )
            except:
                pass


def prepare_commands(commands):
    empty_topic = ''
    ll = []
    for l in commands:
        if type(l) not in (list, tuple):
            ll.append((l, empty_topic))
        else:
            if len(l) > 1:
                ll.append((*l,))
            else:
                ll.append((*l, empty_topic))
    return ll


def qry_commands(serial, commands, stat=None):
    rr = []
    for cmd in commands:
        res = serial.query(cmd[0])
        if not res:
            rr.append((cmd[0], cmd[1], '-'))
            if stat:
                stat.bad_qry += 1
            print('{}-> ???'.format(cmd))
        else:
            rr.append((cmd[0], cmd[1], res))
            print('{}-> {}'.format(cmd, res))
    return rr


def pass_out(mqtt_cfg, data, timestamp, stat=None):
    try:
        if mqtt_cfg:
            now = time.time()
            msgs = []
            for q, t, p in data:
                if t:
                    pl = p
                    if p[0] == '>':
                        pl = p[1:]
                    topic = "{}{}".format(mqtt_cfg["topic"], t)
                    if timestamp:
                        msgs.append(dict(topic=topic, payload="{0} {1:0.6f}".format(pl, now)))
                    else:
                        msgs.append(dict(topic=topic, payload="{}".format(pl)))

            publish.multiple(msgs
                             , hostname=mqtt_cfg["host"]
                             , port=mqtt_cfg["port"]
                             , client_id=mqtt_cfg["client_id"]
                             )
    except:
        if stat:
            stat.errors = stat.errors + 1
        tp, value, traceback = sys.exc_info()
        logging.error("mqtt exception: {}".format(value))


def main():
    import argparse
    parser = argparse.ArgumentParser(description="play with reallab devices")
    parser.add_argument('-c', '--cfg', dest='cfg', help='playbook yaml file', default="playbook.yml")
    parser.add_argument('-b', '--broker', dest='broker', help='mqtt broker config file', default="mqtt.conf")
    parser.add_argument('-l', '--logging', dest='logg', action='count',
                        help='print diagnostic (can be given multiple times)', default=0)
    args = parser.parse_args()

    if args.logg > 3:
        args.logg = 3
    level = (logging.WARNING, logging.INFO, logging.DEBUG, logging.NOTSET)[args.logg]
    logging.basicConfig(level=level)
    logging.getLogger('rs485').setLevel(level)

    with open(args.cfg, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    commands = cfg["commands"]
    if not commands:
        print("No commands.")
        return
    commands = prepare_commands(commands)

    rs485_cfg = cfg["rs485"]
    logging.info('serial: {}'.format(rs485_cfg))
    serial = rs485.RS485(rs485_cfg)

    mqtt_cfg = {}
    if "mqtt" in cfg:
        mqtt_cfg = cfg["mqtt"]
    else:
        if os.path.exists(args.broker):
            with open(args.broker, 'r') as ymlfile:
                mqtt_cfg = yaml.safe_load(ymlfile)
    if "topic" not in mqtt_cfg:
        mqtt_cfg["topic"] = 'gtss/reallab/'
    logging.info('mqtt: {}'.format(mqtt_cfg))

    delay = -1
    if "delay" in cfg:
        delay = cfg["delay"] / 1000.

    timestamp = False
    if "timestamp" in cfg:
        timestamp = cfg["timestamp"]

    # ---------------------------------------------------------
    if delay < 0:
        qry_commands(serial, commands)
    else:
        stat = Statistic()
        count = 0
        while 1:
            count += 1
            t1 = time.time()
            if args.logg:
                print('{}\t -{}-'.format(t1, count))
            rr = qry_commands(serial, commands, stat)
            t2 = time.time()
            pass_out(mqtt_cfg, rr, timestamp, stat)
            t3 = time.time()
            stat.time = datetime.now().isoformat()  # datetime.now().isoformat(timespec='microseconds')
            stat.tick = count
            stat.dt[0] = round(t2 - t1, 6)
            stat.dt[1] = round(t3 - t2, 6)
            stat.dt[2] = round(t3 - t1, 6)
            stat.pass_to_mqtt(mqtt_cfg)
            if args.logg:
                print('{0:0.6f} {1:0.6f} = {2:0.6f}\n'.format(stat.dt[0], stat.dt[1], stat.dt[2]))
            if delay:
                time.sleep(delay)
    # ---------------------------------------------------------
    return


if __name__ == '__main__':
    try:
        print('-----------------------------------------------')
        main()
        print('-----------------------------------------------')
    except (SystemExit, KeyboardInterrupt):
        pass
    except:
        tp, value, traceback = sys.exc_info()
        logging.error("__main__ exception: {}".format(value))
